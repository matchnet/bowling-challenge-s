const axios = require("axios");
const socketUrl = "http://SocketServer-env.bsjpgnx33z.us-east-2.elasticbeanstalk.com";
//const gamingWalletUrl = "https://2ylkwrpf1l.execute-api.us-east-2.amazonaws.com/dev/wallet";

const customValidationMsg = function(message, res) {
    const json_arr = { data: [{ status: 0, message: message }], success: false };
    res.contentType('application/json');
    res.end(JSON.stringify(json_arr));
}
module.exports.customValidationMsg = customValidationMsg;

const sendSocket = (eventName, data, message) => {
    //console.log(message);
    return new Promise((resolve, reject) => {
        axios.post(socketUrl + '/sendSocket', {
                eventName: eventName,
                data: data,
                message: message
            })
            .then(function(response) {
                console.log('socket for ', eventName, ' success');
                resolve(true);
            })
            .catch(function(error) {
                console.log(eventName, ' socket ', error);
                reject(error);
            });
    })

}
module.exports.sendSocket = sendSocket;

const findIfGamingWallet = (gameWallet) => {
    return new Promise((resolve, reject) => {
        let gamingWalletUrl = "https://2ylkwrpf1l.execute-api.us-east-2.amazonaws.com/dev/wallet";
        axios.post(gamingWalletUrl + '/details', {
                gameWalletId: gameWallet,
            })
            .then((response) => {
                //console.log("gaming wallet found success", response.data.statusCode, response.data.body);
                if (response.data.statusCode == 200 && response.data.body) {
                    resolve(response.data.body);
                }
                resolve(false);
            })
            .catch((error) => {
                console.log("error in findIfGamingWallet", error);
                reject(error);
            });
    })
}
module.exports.findIfGamingWallet = findIfGamingWallet;

const creditToGamingWallet = (walletId, amount, whatFor, token) => {
    return new Promise((resolve, reject) => {
        let gamingWalletUrl = "https://2ylkwrpf1l.execute-api.us-east-2.amazonaws.com/dev/wallet";
        console.log("token final", token);
        let options = {
            headers: { 'x-access-token': token }
        };
        axios.post(gamingWalletUrl + '/credit', {
                walletId: walletId,
                amount: amount,
                whatFor: whatFor
            }, options)
            .then((response) => {
                console.log("gaming wallet credit success", response);
                if (response.data.statusCode == 200 && response.data.body) {
                    resolve(response.data.body);
                }
                resolve(false);
            })
            .catch((error) => {
                console.log("error in creditToGamingWallet", error);
                reject(error);
            });
    })
}
module.exports.creditToGamingWallet = creditToGamingWallet;